import { BrowserRouter, Routes, Route } from "react-router-dom";
import LandingPage from "../pages/LandingPage";
import SignIn from "../pages/SignIn";
import ForgotPassword from "../pages/ForgotPassword";
import FindProject from "../pages/FindProject";
import BrowserProject from "../pages/FindProject/BrowserProject";
import AppliedProject from "../pages/FindProject/AppliedProject";
import SignUp from "../pages/SignUp";
import FindProjectDetail from "../pages/FindProjectDetail";
import FindJob from "../pages/FindJob";
import BrowserJob from "../pages/FindJob/BrowserJob";
import AppliedJob from "../pages/FindJob/AppliedJob";
import SavedJob from "../pages/FindJob/SavedJob";
import ResetPassword from "../pages/ResetPassword";

function Routers() {
    return (
        <BrowserRouter>
          <Routes>
             <Route path="/" element={<LandingPage />}/>
             <Route path="/find-project" element={<FindProject />}>
                 <Route index element={<BrowserProject />} />
                 <Route path="browser-project" element={<BrowserProject/>}/>
                 <Route path="applied-project" element={<AppliedProject/>}/>
             </Route>
             <Route path="/find-job" element={<FindJob />}>
                 <Route index element={<BrowserJob />} />
                 <Route path="browser-job" element={<BrowserJob/>}/>
                 <Route path="applied-job" element={<AppliedJob/>}/>
                 <Route path="saved-job" element={<SavedJob />}/>
             </Route>
             <Route path="/signin" element={<SignIn />} />
             <Route path="/sign-up" element={<SignUp />} />
             <Route path="/detail-page" element={ <FindProjectDetail />}/>
             <Route path="/forgot-password" element={<ForgotPassword />}/>
             <Route path="/reset-password" element={<ResetPassword />}/>
          </Routes>
        </BrowserRouter>
    )
}

export default Routers;