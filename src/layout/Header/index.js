import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'
import logo from'../../../src/assets/img/logo-jobfortech.png'
import iconSearch from '../../../src/assets/img/Magnifer.png'
import "../../index.css"
import Sidebar from '../../components/Sidebar'

function Header({auth}) {
    const [active, setActive] = useState(false)
    const [isAuthenticated, setAuthenticated] = useState(false)
    const [searchInput, setSearchInput] = useState(false)
    const [search, setSearch] = useState("")

    const handleSidebar = () => {
          setActive(!active)
    }

    const onChangeInput = (event) => {
          setSearch(event.target.value)
    }

    const handleSearchInput = () => {
          setSearchInput(searchInput => !searchInput)
    }

    const handleSearch = (e) => {
        e.preventDefault()
        console.log(search)
    }

    const handlerLogout = () => {
        localStorage.removeItem('user', auth)
        setAuthenticated(false)
    }

    // useEffect(()=> {
    //    if(auth != null) {
    //       setAuthenticated(true)
    //    }
    // }, [isAuthenticated])
    
    return (
        <>
          <header className="shadow-md z-0">
             <nav className="z-0 container mx-auto flex px-8 py-1 sm:py-3 md:px-16 md:py-0 sm:flex sm:flex-row">
                 <div className="flex sm:flex-row-reverse space-between py-1 md:flex-row md:px-1">
                    <Link to={'/'}>
                        <img 
                            className="my-2 md:mr-6 sm:px-56 md:px-0"
                            src={logo} 
                            alt="logo-jobfortech.png"/>
                    </Link>
                    <form 
                         className={searchInput ? "flex flex-row border border-2 rounded-lg float-left sm:hidden" : "my-auto sm:hidden ml-28"} 
                         onSubmit={handleSearch}>
                           <span 
                              className="my-auto mt-2">
                                 <img 
                                   className="ml-3 mr-2 w-6"
                                   src={iconSearch} 
                                   alt="magnifier.png"
                                   onClick={handleSearchInput}/>
                            </span>
                         {searchInput ? 
                             <input 
                                className="rounded outline-none w-80 text-xs p-2 duration-500 ease-in"
                                type="text"
                                value={search}
                                onChange={onChangeInput}
                                placeholder="Search by project name, company and talent"
                            />: ''}
                        
                    </form>
                    <button className="md:hidden my-auto justify-end" onClick={handleSidebar}>
                        <span className="material-icons-outlined text-2xl text-blue">
                            menu
                        </span>
                    </button>
                    { active === true ? 
                         <div className="w-full flex z-20 fixed left-0 top-0 duration-200 ease-in md:hidden">
                            <div className="pt-3 px-2 sm:px-10 w-8/12 sm:w-[360px] h-[1133px] bg-white">
                                <Link to={'/signin'}>
                                        <button className="rounded-full text-xs font-weight border-solid border-[2px] border-blue w-[47%] py-2 text-blue mr-3">
                                        Sign In
                                    </button>
                                </Link>
                                <button className="rounded-full font-weight text-xs bg-blue w-[47%] py-2 text-white">Sign Up</button>
                                <hr className="border border-3 border-gray mt-4 sm:mt-6 w-full"/>
                                <form 
                                    className="flex flex-row mt-2 border border-2 rounded-lg"
                                    onSubmit={handleSearch}>
                                    <span 
                                        className="my-auto mt-2"
                                        >
                                        <img 
                                            className="ml-1 w-5"
                                            src={iconSearch} 
                                            alt="magnifier.png"/>
                                    </span>
                                    
                                    <input 
                                        className="rounded outline-none w-full text-[10px] p-2"
                                        type="text"
                                        value={search}
                                        onChange={onChangeInput}
                                        placeholder="Search by project name, company and talent"
                                        />
                                    
                                </form>
                
                                <ul className="mt-5">
                                    <li className="text-[14px] sm:text-[18px] py-2 sm:py-3 font-medium border-b-2 hover:border-b-blue hover:text-blue">Find Projects</li>
                                    <li className="text-[14px] sm:text-[18px] py-2 sm:py-3 font-medium border-b-2 hover:border-b-blue hover:text-blue">Find Jobs</li>
                                </ul>
                            </div> 
                            <div className="bg-[#404040]/[0.4] w-1/3 sm:w-3/5">
                                
                            </div>
                        </div>
                      : "" }
                 </div>
                 <div className="hidden ml-14 navbar-collapse md:flex md:flex-grow">
                    <ul className="flex md:flex md:flex-grow flex-row">
                        <li className="float-left px-4 py-4 border-2 border-white hover:border-b-blue hover:text-blue hover:font-medium">
                            <Link className="font-medium hover:text-blue" to={"/"}>
                               Home
                            </Link>
                        </li>
                        <li className="float-left px-4 py-4 border-2 border-white hover:border-b-blue hover:text-blue hover:font-medium">
                            <Link className="font-medium hover:text-blue" to={"/find-project"}>
                              Find Projects
                            </Link>
                        </li>
                        <li className="float-left px-4 py-4 border-2 border-white hover:border-b-blue hover:text-blue hover:font-medium">
                            <Link className="font-medium hover:text-blue" to={"/find-job"}>
                                Find Jobs
                            </Link>
                        </li>
                    </ul>

                    <div className="flex grow-2 md:flex md:flex-grow flex-row justify-end space-x-6 my-auto">
                        <form 
                           className={searchInput ? "flex flex-row border border-2 rounded-lg float-left" : "my-auto"} 
                           onSubmit={handleSearch}>
                           <span 
                              className="my-auto mt-2">
                                 <img 
                                   className="ml-3 mr-2 w-6"
                                   src={iconSearch} 
                                   alt="magnifier.png"
                                   onClick={handleSearchInput}/>
                            </span>
                         {searchInput ? 
                             <input 
                                className="rounded outline-none w-80 text-xs p-2 duration-500 ease-in"
                                type="text"
                                value={search}
                                onChange={onChangeInput}
                                placeholder="Search by project name, company and talent"
                            />: ''}
                        
                        </form>

                        {isAuthenticated ?  
                          <div>
                              <h2>Welcome back, <b className="text-blue">{auth.name}</b>!</h2>
                          </div> 
                           :
                          <div className="flex gap-3">
                            <Link to={'/signin'}><button className="rounded-full font-weight border-solid border-2 border-blue px-5 py-2 text-blue">Sign In</button></Link>
                            <Link to={'/sign-up'}><button className="rounded-full font-weight bg-blue px-5 py-2 text-white">Sign Up</button></Link>
                          </div>
                         }
                    </div>
                </div>
             </nav> 
          </header>
        </>
    )
}

export default Header