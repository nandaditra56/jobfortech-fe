import logo from "../../assets/img/logo-jobfortech-footer.png"

function Footer() {
    return (
        <>
          <footer className="bg-black py-8">
             <div className="container sm:px-10 text-xs font-medium sm:text-md">
                <div className="flex flex-col sm:flex-row justify-center items-center">
                    <img className="w-2/3 sm:w-1/6" src={logo} alt="logo-jobfortech.png"/>
                    <ul className="flex flex-row sm:pl-10">
                        <li className="float-left px-4 py-2 sm:py-4"><a className="text-white" href="/">Contact Us</a></li>
                        <li className="float-left px-4 py-2 sm:py-4"><a className="text-white" href="/">Terms of Service</a></li>
                        <li className="float-left px-4 py-2 sm:py-4"><a className="text-white" href="/">Privacy policy</a></li>
                    </ul>
                </div>
                <p className="text-center py-1 font-lg">© 2023 jobfortech</p>
             </div>
          </footer>
        </>
    )
}

export default Footer;