import { createSlice } from "@reduxjs/toolkit";

const jobAppliedSlice = createSlice({
    name: 'jobApplied',
    initialState: [],
    reducer: {
        addHandlerJob(state, action){
            state.push(action.payload)
        },
        deleteHandlerJob(state, action) {
            state.items.filter(item => item !== action.payload)
        }
    }
})

const { addHandlerJob, deleteHandlerJob} = jobAppliedSlice.actions;

export default jobAppliedSlice.reducer