import { createSlice } from "@reduxjs/toolkit"

const projects = [
    {
        id:1,
        name: 'CloudWorks',
        company: 'NimbusTech Solutions',
        date:'15th Dec 22',
        range: 'IDR 6,5 - 8 JT',
        work:'3',
        location: 'Indonesia',
        description:'A cloud-based platform for managing business workflows and processes, developed by NimbusTech Solutions, a leading provider of enterprise software solutions.',
        tags: ['Mobile', 'Front End', 'Back end', "E-Commerce"],
        be:'2/3',
        fe:'1/3',
        mf:'3/3',
        statusBar: 'see-more'
    },
    {
        id:2,
        name: 'DataFlow',
        company: 'Streamline IT Services',
        date:'15th Dec 22',
        range: 'IDR 6,5 - 8 JT',
        work:'3',
        location: 'Indonesia',
        description:'A data management tool that allows businesses to easily collect, organize, and analyze data, developed by Streamline IT Services, a data management and consulting firm.',
        tags: ['Mobile', 'Front End', 'Back end', "E-Commerce"],
        be:'2/3',
        fe:'1/3',
        mf:'3/3',
        statusBar: 'see-more'
    },
    {
        id:3,
        name: 'CyberShield',
        company: 'ShieldTech Security',
        date:'15th Dec 22',
        range: 'IDR 6,5 - 8 JT',
        work:'3',
        location: 'Indonesia',
        description:'A cybersecurity solution that protects businesses from online threats and attacks, developed by ShieldTech Security, a leading provider of cybersecurity services',
        tags: ['Mobile', 'Front End', 'Back end', "E-Commerce"],
        be:'2/3',
        fe:'1/3',
        mf:'3/3',
        statusBar: 'see-more'
    },
    {
        id:4,
        name: 'CodeWave',
        company: 'CodeWave Software',
        date:'15th Dec 22',
        range: 'IDR 6,5 - 8 JT',
        work:'3',
        location: 'Indonesia',
        description:'A software development platform that streamlines the coding process and enhances collaboration among development teams, developed by CodeWave Software, a software....',
        tags: ['Mobile', 'Front End', 'Back end', "E-Commerce"],
        be:'2/3',
        fe:'1/3',
        mf:'3/3',
        statusBar: 'see-more'
    },
    {
        id:5,
        name: 'WebMaster',
        company: 'WebMaster',
        date:'15th Dec 22',
        range: 'IDR 6,5 - 8 JT',
        work:'3',
        location: 'Indonesia',
        description:'A web design and development tool that enables businesses to create and maintain professional websites, developed by NetMasters, a web design and development agency.',
        tags: ['Mobile', 'Front End', 'Back end', "E-Commerce"],
        be:'2/3',
        fe:'1/3',
        mf:'3/3',
        statusBar: 'see-more'
    },
    {
        id:6,
        name: 'TechConnect',
        company: 'ConnectIT Solutions',
        date:'15th Dec 22',
        range: 'IDR 6,5 - 8 JT',
        work:'3',
        location: 'Indonesia',
        description:'A connectivity platform that integrates various business applications and tools, developed by ConnectIT Solutions, an IT consulting and integration firm.',
        tags: ['Mobile', 'Front End', 'Back end', "E-Commerce"],
        be:'2/3',
        fe:'1/3',
        mf:'3/3',
        statusBar: 'see-more'
    },
]

const projectSlice = createSlice({
    name:'projects',
    initialState: projects,
    reducer: {}
})

export default projectSlice.reducer