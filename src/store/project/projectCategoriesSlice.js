import { createSlice } from "@reduxjs/toolkit";

import category1 from '../../../src/assets/img/icon-category/icon-park.png'
import category2 from '../../../src/assets/img/icon-category/tabler_shopping-bag.png'
import category3 from '../../../src/assets/img/icon-category/fluent_arrow-clockwise-28-filled.png'
import category4 from '../../../src/assets/img/icon-category/fluent_wallet-credit-card-32-regular.png'
import category5 from '../../../src/assets/img/icon-category/iconoir_healthcare.png'
import category6 from '../../../src/assets/img/icon-category/carbon_agriculture-analytics.png'
import category7 from '../../../src/assets/img/icon-category/carbon_blockchain.png'
import category8 from '../../../src/assets/img/icon-category/healthicons_group-discussion-meeting-outline.png'

const projectCategories = [
    {
      id:1,
      title:"Education Technology",
      img:category1,
      text:"Online Learning, Web Seminar, Online Assessment, Virtual Campus",
      alt:"category-1.png"
    },
    {
      id:2,
      title:"E-Commerce and Marketplace",
      img:category2,
      text:"Product SKU Management, Stock Management, Product Catalog, etc",
      alt:"category-2.png"
    },
    {
      id:3,
      title:"Operation and Supply Chain",
      img:category3,
      text:"Purchasing, Warehouse Management, Sales Management,etc",
      alt:"category-3.png"
    },
    {
      id:4,
      title:"Financial Technology",
      img:category4,
      text:"Loan Origination System, E-KYC, Credit Scoring, Payment Gateway,etc",
      alt:"category-4.png"
    },
    {
      id:5,
      title:"Healthcare",
      img:category5,
      text:"Advanced analytics, Digitalization, Digital Commercy, etc",
      alt:"category-5.png"
    },
    {
      id:6,
      title:"Agriculture",
      img:category6,
      text:"Digitalization, Big Data, Digital Commerce, Web & Mobile Apps, etcc",
      alt:"category-6.png"
    },
    {
      id:7,
      title:"Web 3.0 and Blockchain Tech",
      img:category7,
      text:"Smart Contract Development, Blockchain Network Integration, etc",
      alt:"category-7.png"
    },
    {
      id:8,
      title:"Communication",
      img:category8,
      text:"Web and Mobile Apps, AR and VR, AI and Machine Learning, Big Data, etc",
      alt:"category-8.png"
    },        
  ]  

const projectCategoriesSlice = createSlice({
    name:'projectCategories',
    initialState: projectCategories,
    reducers: {}
})

export default projectCategoriesSlice.reducer