import { createSlice } from "@reduxjs/toolkit";
import item1 from '../../assets/img/icon-popular-talent/Frame 5.svg'
import item2 from '../../assets/img/icon-popular-talent/Frame 16.svg'
import item3 from '../../assets/img/icon-popular-talent/Frame 6.svg'
import item4 from '../../assets/img/icon-popular-talent/Frame 7.svg'
import item5 from '../../assets/img/icon-popular-talent/Frame 8.svg'
import item6 from '../../assets/img/icon-popular-talent/Frame 9.svg'

const popularTalentItems = [
    {
      id:1,
      title:"User Interface (UI) Designer",
      img:item1,
      description:"Creating visually appealing & intuitive user interfaces."
    },
    {
      id:2,
      title:"User Experience (UX) Designer",
      img:item2,
      description:"Creating intuitive & user-friendly product design.",
    },
    {
      id:3,
      title:"UX Copywriter",
      img:item3,
      description:"Creating intuitive & user-friendly product design."
    },
    {
      id:4,
      title:"Illustrator",
      img:item4,
      description:"Creating intuitive & user-friendly product design.",
    },
    {
      id:5,
      title:"Back-End Developer",
      img:item5,
      description:"Designing and implementing server-side architecture, writing clean and efficient code."
    },
    {
      id:6,
      title:"Full Stack Developer",
      img:item6,
      description:"Developing and maintaining both server-side and client-side of products.",
    },
    
  ]

const popularTalentsSlice = createSlice({
    name:'popularTalents',
    initialState: popularTalentItems,
    reducers: {}
})

export default popularTalentsSlice.reducer 