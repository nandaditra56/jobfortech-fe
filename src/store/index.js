import { configureStore } from '@reduxjs/toolkit'
import jobAppliedReducer from './applied/jobAppliedSlice'
import projectCategoriesReducer from './project/projectCategoriesSlice'
import projectReducer from './project/projectSlice'
import popularTalentsReducer from './talent/popularTalentsSlice'
import jobOfferReducer from './job/jobOfferSlice'
import jobsReducer from './job/jobSlice'
import authReducer from './auth/authSlice'

export default configureStore({
  reducer: {
    projectCategories: projectCategoriesReducer,
    popularTalents: popularTalentsReducer,
    jobOffers : jobOfferReducer,
    jobs: jobsReducer,
    appliedJob: jobAppliedReducer,
    projects : projectReducer,
    auth: authReducer
  },
})