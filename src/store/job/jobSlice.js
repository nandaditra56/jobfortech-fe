import { createSlice } from "@reduxjs/toolkit"

const jobOfferItems = [
    {
      id:1,
      name:'Software Engineer',
      company:'ABC Tech',
      color:'blue',
      location:'San Francisco, USA',
      work:'Hybrid'
    },
    {
      id:2,
      name:'Marketing Manager',
      company:'XYZ Corp',
      color:'green',
      location:'London, UK',
      work:'Remote'
    },
    {
      id:3,
      name:'Data Analyst',
      company:'Acme Inc',
      color:'orange',
      location:'Toronto, Canada',
      work:'On-site'
    },
    {
      id:4,
      name:'UI/UX Designer',
      company:'Blue Sky',
      color:'blue',
      location:'Sydney, Australia',
      work:'Hybrid'
    },
    {
      id:5,
      name:'Graphic Designer',
      company:'Green Leaf',
      color:'orange',
      location:'Paris, France',
      work:'On-site'
    }
  ]

  const jobsSlice = createSlice({
    name: 'jobs',
    initialState: jobOfferItems,
    reducers: {}
  })

  export default jobsSlice.reducer;