import { createSlice } from "@reduxjs/toolkit"

const jobOffers = [
    {
      id:1,
      name:'Software Engineer',
      company:'ABC Tech',
      color:'blue',
      location:'San Francisco, USA',
      work:'Hybrid'
    },
    {
      id:2,
      name:'Marketing Manager',
      company:'XYZ Corp',
      color:'green',
      location:'London, UK',
      work:'Remote'
    },
    {
      id:3,
      name:'Data Analyst',
      company:'Acme Inc',
      color:'orange',
      location:'Toronto, Canada',
      work:'On-site'
    }
  ]

  const jobOffersSlice = createSlice({
      name: 'jobOffer',
      initialState:jobOffers,
      reducer: {}
  })

  export default jobOffersSlice.reducer