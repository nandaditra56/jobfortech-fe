export default {
    fetchToken: async () => {
        try {
            const res= await fetch("http://localhost:3000/signin", {
                credentials: 'include',
            });
            const data = await res.json()
            return data.csrf_auth_token
       } catch (error) {
           console.log(error);
       }
    },
    logOut: async(token)=> {
        try {
            const res= await fetch("http://localhost:3000/logout", {
                credentials: 'include',
            });
            const data = await res.json()
            return data.csrf_auth_token
       } catch (error) {
           console.log(error);
       } 
    }
}