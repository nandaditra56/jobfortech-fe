import { createContext, useState } from "react";

export const UserContext = createContext();

export default ({children}) => {
    const [user, setUser] = useState("");
    const [token, setToken] = useState("");
    const [authentication, setAuthentication] = useState(false);

    return (
       <UserContext.Provider 
          value={{ 
            user, 
            setUser,
            token,
            setToken,
            authentication,
            setAuthentication,
          }}
        >
            {children}
        </UserContext.Provider>
    )
}