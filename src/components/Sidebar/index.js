import { Link } from 'react-router-dom'
import { useState } from 'react'
import iconSearch from '../../../src/assets/img/Magnifer.png'

function Sidebar() {
    const [search, setSearch] = useState("")

    const onChangeInput = (event) => {
        setSearch(event.target.value)
    }

    const handleSearch = (e) => {
        e.preventDefault()
        console.log(search)
    }
 
    
    return (
         <div className="w-full flex z-20 fixed left-0 top-0 duration-200 ease-in">
            <div className="pt-3 px-2 sm:px-10 w-8/12 sm:w-[360px] h-[1133px] bg-white">
                <Link to={'/signin'}>
                        <button className="rounded-full text-xs font-weight border-solid border-[2px] border-blue w-[47%] py-2 text-blue mr-3">
                        Sign In
                    </button>
                </Link>
                <button className="rounded-full font-weight text-xs bg-blue w-[47%] py-2 text-white">Sign Up</button>
                <hr className="border border-3 border-gray mt-4 sm:mt-6 w-full"/>
                <form 
                    className="flex flex-row mt-2 border border-2 rounded-lg"
                    onSubmit={handleSearch}>
                    <span 
                        className="my-auto mt-2"
                        >
                        <img 
                            className="ml-1 w-5"
                            src={iconSearch} 
                            alt="magnifier.png"/>
                    </span>
                    
                    <input 
                        className="rounded outline-none w-full text-[10px] p-2"
                        type="text"
                        value={search}
                        onChange={onChangeInput}
                        placeholder="Search by project name, company and talent"
                        />
                    
                </form>

                <ul className="mt-5">
                    <li className="text-[14px] sm:text-[18px] py-2 sm:py-3 font-medium border-b-2 hover:border-b-blue hover:text-blue">Find Projects</li>
                    <li className="text-[14px] sm:text-[18px] py-2 sm:py-3 font-medium border-b-2 hover:border-b-blue hover:text-blue">Find Jobs</li>
                </ul>
            </div> 
            <div className="bg-[#404040]/[0.4] w-1/3">
                 
            </div>
        </div>
    )
}

export default Sidebar