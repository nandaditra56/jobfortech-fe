import React, { useState } from 'react'
import map from '../../../src/assets/img/icon-job-offer/place.png'
import suitcase from '../../../src/assets/img/icon-job-offer/Suitcase.png'
import bookmark1 from '../../../src/assets/img/icon-job-offer/Bookmark.png'
import bookmark2 from '../../../src/assets/img/icon-job-offer/Bookmark.svg'

function JobApplied({props}){
    const [active, setActive] = useState(false);
    
    const handleActive = () => {
         setActive(active => !active)
    }
    
    const colorVariants = {
        blue: 'bg-blue',
        green: 'bg-green',
        orange: 'bg-orange'
    }

    return (
        <div className="col-span-1 flex p-5 border border-2 rounded-lg shadow-md">
            <div className="mt-3 mr-2">
                <span 
                    className={`rounded px-5 py-3 ${colorVariants[props.color]} rounded-lg`}>
                </span>
            </div>
            <div className="pr-4 w-full">
                <h1 className="font-bold text-sm md:text-xl text-black-300">{props.name}</h1>
                <p className="text-xs">{props.company}</p>
                <img 
                   className="float-left" 
                   src={map} 
                   alt="map"/>
                     <span className="p-2 text-xs">{props.location}</span><br />
                <img 
                   className="float-left" 
                   src={suitcase} 
                   alt="suitcase.png"/>
                   <span className="p-2 text-xs">{props.work}</span>
            </div>
            <div>
                {active ? 
                  <img 
                     onClick={handleActive}
                     src={bookmark2}
                     alt="bookmark"
                  />
                  :
                  <img 
                    onClick={handleActive} 
                    src={bookmark1}
                    alt='bookmark'
                 />}
            </div>
        </div>
    )
}

export default JobApplied