import { buttonStyle } from "../buttonStyle"

function ButtonApplied(){
    return (
        <button className={`${buttonStyle} text-[#858585] bg-[#D9D9D9]`}>Waiting Approval</button>
    )
}

export default ButtonApplied