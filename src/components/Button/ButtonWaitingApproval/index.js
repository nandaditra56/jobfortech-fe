import waiting from '../../assets/img/find-project/applied-project/waiting.svg'
import { buttonStyle } from '../buttonStyle'

function ButtonWaitingApproval(){
    return (
        <button className={`${buttonStyle} flex justify-center text-[#FF8845] bg-[#FFEFE7]`}>
                  Waiting Approval 
                 <img src={waiting} width={20} alt="waiting" className="mx-3"/>
       </button>
    )
}

return ButtonWaitingApproval