import { buttonStyle } from "../buttonStyle"

function ButtonRejected() {
    return (
        <button className={`${buttonStyle} text-[#BC251C] bg-[#EEB4B0]`}>Rejected</button>
    )
}

export default ButtonRejected