import logo from '../../assets/img/find-project/browser-project/icon-project.png'
import map from '../../assets/img/find-project/browser-project/Map Point.svg'
import money from '../../assets/img/find-project/browser-project/Dollar Minimalistic.svg'
import clock from '../../assets/img/find-project/browser-project/Clock Circle.svg'
import waiting from '../../assets/img/find-project/applied-project/waiting.svg'

const buttonStyle = "font-semibold rounded-full py-2 w-full"
const ButtonApplied = <button className={`${buttonStyle} text-[#858585] bg-[#D9D9D9]`}>Waiting Approval</button>
const ButtonRejected = <button className={`${buttonStyle} text-[#BC251C] bg-[#EEB4B0]`}>Rejected</button>
const ButtonWaitingApproval = <button className={`${buttonStyle} flex justify-center text-[#FF8845] bg-[#FFEFE7]`}>
                                    Waiting Approval 
                                    <img src={waiting} width={20} alt="waiting" className="mx-3"/>
                             </button>

function ProjectItem({props}) {
   
   return (
        <div key={props.id} className="border-2 rounded-xl shadow-xl p-5 sm:p-7">
            <div>
                <img
                src={logo}
                alt="logo"
                width={48}
                className="float-left mb-4 mr-6"
                />
                <div>
                    <h1 className="text-blue text-[20px] font-semibold">{props.name}</h1>
                    <p className="text-[14px] font-regular text-[#404040]">{props.company}</p>
                    <i className="text-xs">Posted on {props.date}</i>
                </div>
            </div>
            <div className="flex flex-col sm:flex-row mt-4">
                <div className="w-full">
                    <h4 className="text-xs sm:text-[14px] font-medium text-[#404040]">Monthly</h4>
                    <div className="my-auto">
                    <img 
                            src={money}
                            className="float-left mt-1"
                            alt="logo"
                    />
                    <span className="ml-2 text-[11px] sm:text-[12px] text-[#111111]">{props.range}</span>
                    </div>
                </div>
                <div className="w-full">
                    <h4 className="text-xs sm:text-[14px] font-medium text-[#404040]">Work Time</h4>
                    <div className="my-auto">
                    <img 
                            src={clock}
                            className="float-left mt-1"
                            alt="logo"
                    />
                    <span className="ml-2 text-[11px] sm:text-[12px] text-[#111111]">{props.work} Months Development</span>
                    </div>
                </div>
                <div className="w-full">
                    <h4 className="text-xs sm:text-[14px] font-medium text-[#404040]">Location</h4>
                    <div className="my-auto">
                    <img 
                            src={map}
                            className="float-left mt-1"
                            alt="logo"
                    />
                    <span className="ml-2 text-[11px] sm:text-[12px] text-[#111111]">{props.location}</span>
                    </div>
                </div>
            </div>

            <p className="text-[11px] sm:text-xs text-[#404040] py-4">{props.description}</p>

            <div className="text-blue text-[10px] sm:text-xs">
                <span className="border border-2 border-blue rounded-full px-2 sm:px-2">Mobile</span>
                <span className="border border-2 border-blue rounded-full px-2 sm:px-2">Front End</span>
                <span className="border border-2 border-blue rounded-full px-2 sm:px-2">Back End</span>
                <span className="border border-2 border-blue rounded-full px-2 sm:px-2">E-Commerce</span>
            </div>

            <div className="flex text-[16px] gap-5 my-3">
                <p>BE : <b className="text-blue">{props.be}</b></p>
                <p>FE : <b className="text-blue">{props.fe}</b></p>
                <p>MF : <b className="text-blue">{props.mf}</b></p>
            </div>

            { props.statusBar === 'waiting-approval' ? ButtonWaitingApproval :
              props.statusBar === 'rejected' ? ButtonRejected  :
              props.statusBar === 'applied' ? ButtonApplied  :
             <button className={`${buttonStyle} text-white bg-blue`}>See More</button>
            }
        </div>
   )
}

export default ProjectItem