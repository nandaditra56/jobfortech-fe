import { Link } from "react-router-dom"
import icon from "../../assets/img/find-project/login-applied.svg"

function AppliedSignIn({description}){
    return (
        <div className="container mx-auto p-16 md:p-32 flex justify-center items-center ">
            <div className="text-center md:w-[39%] pb-28">
                 <section className="flex justify-center mb-7">
                    <img 
                        width={450}
                        height={450}
                        src={icon}
                        alt="not-found"
                    />
                 </section>
                 <h3 className="text-2xl font-semibold text-blue">{description}</h3>
                 <p className="text-[12px] md:text-[14px] mt-1">To access this feature, please log in to your account or sign up for free today and start exploring our platform</p>
                 <Link to={'/signin'}>
                    <button className="mt-10 text-white text-[18px] font-semibold bg-blue rounded-full px-10 py-2">
                         Sign in now
                    </button>
                </Link>
            </div>
        </div>
    )
}

export default AppliedSignIn