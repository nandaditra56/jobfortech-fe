function PopularJobItem({props}) {
    return (
        <div className="flex border border-1 p-1 relative">        
            <div className="flex w-100% py-1 sm:w-[70%] md:w-[85%]">
                <img 
                    className="mx-3 w-14 p-1 rounded-full"
                    src={props.img} 
                    alt={props.title} />
                <div className="pl-1">
                    <h5 className="text-blue text-xs sm:text-sm md:text-xl font-semibold">{props.title}</h5>
                    <p className="text-xs sm:text-sm md:text-xl font-medium">{props.description}</p>
                </div>
            </div>
            <div className="my-auto"> 
                <button className="bg-blue text-white font-bold rounded-md py-2 px-10">See Projects</button>
            </div>
        </div>
   )
}

export default PopularJobItem