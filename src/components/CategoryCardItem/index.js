import React from "react"

function CategoryCardItem({ props } ) {
    return (
        <div className="col-span-1 border border-2 border-blue rounded-lg py-3 sm:py-4 md:py-5 px-1 md:px-8 text-center shadow-md">
            <div className="flex flex-row justify-center items-center">
                <img  
                    className="rounded-lg bg-gray-300 mt-1 p-1 md:p-2 w-9 sm:w-14 md:w-14" 
                    src={props.img}
                    alt={props.alt}
                    />
                </div>
            <h3 className="text-blue text-[14px] sm:text-base md:text-xl mt-2 sm:my-3 font-semibold">{props.title}</h3>
            <p className="text-[10px] sm:text-[12px] md:text-[13px]">{props.text}</p>
        </div>
    )
}

export default CategoryCardItem