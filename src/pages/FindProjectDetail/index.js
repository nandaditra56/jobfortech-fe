import Footer from "../../layout/Footer"
import Header from "../../layout/Header"
import map from '../../assets/img/find-project/browser-project/Map Point.svg'
import money from '../../assets/img/find-project/browser-project/Dollar Minimalistic.svg'
import clock from '../../assets/img/find-project/browser-project/Clock Circle.svg'
import logo from '../../assets/img/find-project/browser-project/icon-project.png'

function FindProjectDetail() {
    return (
        <>
          <Header />

          <div className="container mx-auto "> 
             <div className="md:px-32 pt-10 pb-40">
                <div className="flex py-2 text-blue gap-2">
                    <span class="material-icons-outlined">west</span><b className="text-[16px] "> Back</b>
                </div>
                <div className="md:flex gap-4 my-5">
                <div className="w-[100%] md:w-[68%]">
                        <div className="block mb-4">
                            <img
                                src={logo}
                                alt="logo"
                                width={61}
                                className="float-left mb-4 mr-6"
                            />
                            <div>
                                <h1 className="text-blue text-[20px] font-semibold">DataFlow</h1>
                                <p className="text-[14px] font-regular text-[#404040]">Streamline IT Services</p>
                                <i className="text-xs">Posted on 15th Dec 22</i>
                            </div>
                        </div>
                        <h3 className="text-[#404040] font-semibold text-[14px]">Desciption</h3>
                        <p className="pt-2 text-xs text-justify"> Our company is looking to develop an online marketplace platform that connects buyers and sellers from around the world. The platform will allow sellers to create their own online stores, list their products, and manage their inventory. Buyers will be able to browse products, compare prices, and make purchases securely through the platform. The platform will also offer a range of features such as search filters, ratings and reviews, and payment processing. The project will involve building the platform from scratch, including the website design, backend development, and database integration.</p>
                
                        <div className="flex text-blue text-[10px] sm:text-xs gap-2 my-3">
                            <div className="border border-2 border-blue rounded-full px-2 sm:px-2">Mobile</div>
                            <div className="border border-2 border-blue rounded-full px-2 sm:px-2">Front End</div>
                            <div className="border border-2 border-blue rounded-full px-2 sm:px-2">Back End</div>
                            <div className="border border-2 border-blue rounded-full px-2 sm:px-2">E-Commerce</div>
                        </div>

                        <div className="flex border-2 my-12 py-5 border-white border-y-blue gap-4">
                            <div className="w-full border-2 border-white border-r-blue">
                                <h4 className="text-xs sm:text-[14px] font-semibold text-[#404040] mb-1">Monthly</h4>
                                <div className="my-auto">
                                <img 
                                        src={money}
                                        className="float-left mt-1"
                                        alt="logo"
                                />
                                <span className="ml-2 text-[11px] sm:text-[12px] text-[#111111]">IDR 6,5 - 8 JT</span>
                                </div>
                            </div>
                            <div className="w-full border-2 border-white border-r-blue">
                                <h4 className="text-xs sm:text-[14px] font-semibold text-[#404040] mb-1">Work Time</h4>
                                <div className="my-auto">
                                <img 
                                        src={clock}
                                        className="float-left mt-1"
                                        alt="logo"
                                />
                                <span className="ml-2 text-[11px] sm:text-[12px] text-[#111111]">3 Months Development</span>
                                </div>
                            </div>
                            <div className="w-full">
                                <h4 className="text-xs sm:text-[14px] font-semibold text-[#404040] mb-1">Location</h4>
                                <div className="my-auto">
                                <img 
                                        src={map}
                                        className="float-left mt-1"
                                        alt="logo"
                                />
                                <span className="ml-2 text-[11px] sm:text-[12px] text-[#111111]">Indonesia</span>
                                </div>
                            </div>
                        </div>

                        <h4 className="text-xs sm:text-[14px] font-semibold text-[#404040] mb-1">Team Profile</h4>
                        <div className="flex w-[100%] gap-3">
                            <div className="pr-3 border-2 border-white border-r-blue">
                                <div className="flex">
                                <div>
                                    <h4>Front End Dev</h4>
                                    </div> 
                                <b className="pl-14 text-[24px] text-blue">1/3</b>
                                </div>
                                <i className="text-blue text-[10px]">2 position left as a Mobile Developer!</i>
                            </div>
                            <div className="pr-3 border-2 border-white border-r-blue">
                                <div className="flex">
                                <div>
                                    <h4>Back End Dev</h4>
                                    </div> 
                                <b className="pl-14 text-[24px] text-blue">2/3</b>
                                </div>
                                <i className="text-blue text-[10px]">1 position left as a Back End Dev!</i>
                            </div>
                            <div className="pr-3 border-2 border-white border-r-blue">
                                <div className="flex">
                                <div>
                                    <h4>Front End Dev</h4>
                                    </div> 
                                <b className="pl-14 text-[24px]">3/3</b>
                                </div>
                                <i className="text-[10px]">All position has been filled!</i>
                            </div>
                        </div>
                </div>
                <div className="w-[100%] md:w-[32%]">
                    <div className="p-4 border border-2 shadow-xl rounded-xl">
                        <h5 className="text-[14px] font-semibold text-[#404040]">Apply to Project</h5>

                        <p className="my-4 text-xs">Choose the desired position you want to apply for</p>

                        <select id="role" class="bg-white w-full p-3 rounded-md border border-2 border-[#E0E0E0]">
                            <option value="Front End Developer"selected>Front End Developer</option>
                            <option value="Backend Developer">Backend Developer</option>
                            <option value="Mobile Developer">Mobile Developer</option>
                        </select>

                        <p className="text-blue text-[10px] my-4"><i>2 position left as a Mobile Developer!</i></p>

                        <div className="mb-5">
                                <input
                                    type="checkbox"
                                    name="term-condition"
                                    className="p-5 border-blue border-2"
                                />
                                <label className="text-xs ml-5">
                                    I agree with the <b className="text-blue">Terms and Conditions</b> 
                                </label><br/>

                                <input
                                    type="checkbox"
                                    name="privacy-policy"
                                    className="p-5 border-blue border-2"
                                />
                                <label className="text-xs ml-5">I agree with the <b className="text-blue">Privacy Policy</b></label><br/>                        
                        </div>

                        <button className="bg-blue text-white p-3 font-bold w-full rounded-md" type="submit">Apply</button>
                        </div>
                    </div>
                </div>
             </div>
          </div>

          <Footer />
        </>
    )
}

export default FindProjectDetail