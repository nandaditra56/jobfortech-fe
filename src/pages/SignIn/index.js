import { useState } from "react"
import { Link, useNavigate } from "react-router-dom"
import Header from "../../layout/Header"
import Footer from "../../layout/Footer"
import image from "../../assets/img/signin/Illustration.svg"
import google from "../../assets/img/signin/logos_google-icon.svg"
import facebook from "../../assets/img/signin/logos_facebook.svg"

function SignIn() {
    const [ authenticated, setAuthenticated] = useState(false) 
    const [ email, setEmail] = useState("")
    const [ password, setPassword] = useState("")
    const [ error, setError] = useState(false)
    const [ message, setMessage] = useState("");
    const navigate = useNavigate();

    // mock data 
    const emailData = "john@gmail.com"
    const passwordData = "test123"

    const onChangeInput = (event) => {
        const {name, value} = event.target
        console.log(name , value)
    }



    const submitHandleForm = (e) => {
        e.preventDefault()

        // if(!validateData(user.email, user.password)) {
        //    navigate('/signin')
        // }

        // if(validateData(user.email, user.password)) {
        //    navigate('/')
        // }
    }
    
    return (
        <>
          <Header/>

          
          <div className="container mx-auto px-9 sm:px-12 md:px-32 ">
                 <div className="grid grid-cols-2 py-8 ms:py-12 md:py-32">
                     <div className="col-span-2 md:col-span-1 my-8">
                        <h1 className="text-blue text-2xl sm:text-4xl font-bold">Sign In</h1>
                        <p className="text-sm sm:text-xl font-medium my-5">Welcome back! Sign in to <Link to={"/"} className="text-blue"><b>jobfortech</b></Link> and start applying for new freelance projects.</p>
                        <div className="border border-1 mt-3 sm:mr-8 rounded-md">
                            <div className="px-4 sm:py-8 sm:px-9">
                                 <form >
                                    <div className="mt-4 sm:mt-5">
                                        <label 
                                            htmlFor="email" 
                                            className="text-blue text-sm sm:text-base font-semibold">
                                                Email
                                        </label><br />
                                        <input 
                                            className="p-[0.5em] sm:p-2 w-full text-sm border border-gray border-2 rounded-md"
                                            name="email" 
                                            type="text"
                                            placeholder="Insert your email"
                                            /><br /> 
                                    </div>
                                    <div className="mt-5">
                                        <label 
                                            htmlFor="password" 
                                            className="text-sm sm:text-base text-blue font-semibold">
                                                Password
                                        </label><br/>
                                        <input 
                                            className={error ? " bg-[#EEB4B0] p-[0.5em] placeholder-[#E46F67] sm:p-2 w-full border border-gray border-2 rounded-md" : "p-[0.5em] sm:p-2 w-full border border-gray border-2 rounded-md"}
                                            name="password"
                                            type="text"
                                            placeholder="Insert your password"
                                            />
                                       {error === true ? <p className="text-xs text-[#BD251C] py-1">*Please fill out all required fields before submitting the form.</p> : ""}
                                         
                                    </div>
                                    <Link to={'/forgot-password'}><p className="text-right text-xs sm:text-sm font-normal text-blue my-4">Forgot Password?</p></Link>
                                    <button className="bg-blue w-full p-2 rounded-md text-white font-semibold text-base" type="submit">Sign in</button>

                                    <div className="flex my-5 text-center">
                                        <hr className="border border-3 border-gray m-auto w-full" /><span className="w-80 m-auto text-xs">or sign in with</span><hr className="border border-3 border-gray m-auto w-full"/>
                                    </div>
                                
                                    <div className="flex sm:flex-row flex-col gap-4">
                                        <button className="w-full p-2 flex border border-gray rounded-md justify-center">
                                             <img 
                                                className="mx-3" 
                                                src={google} 
                                                alt="google-icon.png"/> 
                                                Google
                                        </button>
                                        <button className="w-full p-2 flex border border-gray rounded-md justify-center">
                                             <img 
                                                className="mx-3" 
                                                src={facebook} 
                                                alt="facebook-icon.png"/>
                                                 Facebook
                                        </button>
                                    </div>
                                    <p className="font-medium text-center mt-5 text-xs sm:text-sm">Didn't have an account? <b className="text-blue">Sign Up</b></p>
                                 </form>
                            </div>
                        </div>
                     </div>
                     <div className="hidden md:block md:col-span-1 md:p-5">
                         <img
                            className="w-full" 
                            src={image} 
                            alt="ilustration-signin.png"/>
                     </div>
                 </div>
             </div>

          <Footer />
        </>
    )
}

export default SignIn