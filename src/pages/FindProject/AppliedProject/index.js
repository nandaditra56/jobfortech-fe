import AppliedSignIn from "../../../components/AppliedSignIn"
import NotFound from "../../NotFound"

const description = "Get your next project by sign in to our website!"

function AppliedProject() {
    return (
        // <NotFound />
        <AppliedSignIn description={description} />
    )
}

export default AppliedProject