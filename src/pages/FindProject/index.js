import React from 'react'
import Header from '../../layout/Header'
import Footer from '../../layout/Footer'
import search from '../../assets/img/find-project/search-icon.svg'
import image from '../../assets/img/find-project/find-project-bg.svg'
import { Outlet, NavLink } from 'react-router-dom'

function FindProject() {
    return (
      <>
          <Header />
          
          <div className="pb-10">
              <div className="container mx-auto md:flex md:bg-auto bg-cover bg-radial-new bg-no-repeat bg-bottom-4 md:bg-top-1">
                <div className="w-[100%] md:w-[40%] pl-9 sm:pl-20 md:pl-32 py-10 md:py-20">
                   <h1 className="text-blue text-[20px] sm:text-[40px] font-bold">Find Your Project Now!</h1>
                   <p className="font-medium text-xs sm:text-2xl">Find your perfect project match with our comprehensive search and filtering tools</p>
                </div>
                <div className="w-[100%] md:w-[60%] bg-find-project sm:bg-none md:bg-find-project bg-no-repeat sm:pt-10 sm:pt-0 md:bg-top-4 sm:relative">              
                    <img 
                       src={image}
                       alt="find-project"
                       className="sm:block md:hidden mx-auto"
                    />
               </div>
              </div>
              <div className="container mx-auto px-5 md:px-32 pt-10 z-10">
                   <div className="border-2 rounded-xl shadow-lg">
                        <div className="p-5">
                            <ul className="flex text-xs sm:text-xl font-semibold text-black-300">
                                <li className="px-3 sm:px-7 sm:py-2 hover:border-b-4 border-blue hover:text-blue"><NavLink to={"/find-project/browser-project"}>Browse</NavLink></li>
                                <li className="px-3 sm:px-7 sm:py-2 hover:border-b-4 border-blue hover:text-blue"><NavLink to={"/find-project/applied-project"}>Applied</NavLink></li>
                            </ul>
                            <button className="w-full text-blue border p-2 mt-4 border-2 border-blue rounded-xl font-bold sm:hidden">
                                Filter
                            </button>
                            <div className="bg-[#F5F5F5] mt-3 rounded-2xl hidden sm:block">
                                <div className="py-3 px-7">
                                    <form className="flex flex-row gap-5">
                                        <div className="flex md:flex-row flex-col w-[100%] gap-4">
                                            <div className="md:w-[100%]">
                                                <p className="text-xs text-[#404040]">Search</p>
                                                <div className="w-full flex bg-white border-2 border-[#E0E0E0] rounded-lg">
                                                    <img 
                                                        src={search}
                                                        alt="icon-search"
                                                        width={50}
                                                        className='px-4'
                                                    />
                                                    <input
                                                        name="name"
                                                        type="text"
                                                        placeholder="search project name, Talent, Company etc"
                                                        className="border-white bg-white outline-none text-[12px] md:text-[14px] focus:ring-0 w-full py-2 rounded-lg"
                                                    />
                                                </div>                                           
                                            </div>
                                            <div className="flex gap-4 mt-2 md:mt-0 w-full">
                                                <div className="w-[50%]"> 
                                                    <p className="text-xs text-[#404040]">Category</p>
                                                    <select name="category" className="bg-white focus:ring-0 w-full py-2.5 px-2 border border-2 rounded-lg font-bold text-black">
                                                        <option value="all">All</option>
                                                        <option value="saab">Saab</option>
                                                        <option value="opel">Opel</option>
                                                        <option value="audi">Audi</option>
                                                    </select>
                                                </div>
                                                <div className="w-[50%]"> 
                                                    <p className="text-xs text-[#404040]">Talent</p>
                                                    <select className="bg-white focus:ring-0 w-full py-2.5 px-2 border border-2 rounded-lg font-bold text-black">
                                                        <option value="all">All</option>
                                                        <option value="saab">Saab</option>
                                                        <option value="opel">Opel</option>
                                                        <option value="audi">Audi</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <button 
                                            type="submit"
                                            className="px-10 py-1 my-10 md:my-2 bg-blue rounded-lg text-white"
                                            >
                                            Filter
                                        </button>
                                    </form>
                                </div> 
                            </div>
                        </div>
                   </div>
                </div>

                <Outlet />
          </div>

          <Footer />
      </>
    )
}

export default FindProject