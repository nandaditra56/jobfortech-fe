import Footer from "../../layout/Footer"
import Header from "../../layout/Header"
import { useState } from "react"

function ResetPassword(){
    const [ authenticated, setAuthenticated] = useState(false) 
    const [ user, setUser] = useState({email:'', password:''})
    const [ error, setError] = useState(false)
    const [ message, setMessage] = useState("");
    const [ resetPassword, setResetPassword] = useState(false)

    const onChangeInput = (e) => {
        const {name, value} = e.target
        setUser({ ...user, [name]: value })
    }

    const handleClickProcessPassword = () => {
        
    }

    return (
        <>
           <Header />
           
           <div className="container mx-auto px-9 sm:px-2 md:px-32 py-14 sm:py-32 md:py-40 bg-radial-auth bg-no-repeat bg-center"> 
                <div className="mx-auto my-14 bg-white border border-gray border-3 p-9 ms:p-10 md:w-1/2 text-center rounded-xl shadow-md">
                      <h1 className="text-xl sm:text-3xl font-bold text-blue my-2">Reset Password</h1>
                      <p className="text-left text-xs sm:text-xl mt-5">Please enter your new password below, making sure it meets the minimum requirements for length and complexity.</p>
            
                      <form className="text-left">
                           <div className="mt-5">
                                    <label 
                                        htmlFor="new-password" 
                                        className="text-sm sm:text-base text-blue font-semibold">
                                            New Password
                                    </label><br/>
                                    <input 
                                        className={error ? " bg-[#EEB4B0] p-[0.5em] placeholder-[#E46F67] sm:p-2 w-full border border-gray border-2 rounded-md" : "p-[0.5em] sm:p-2 w-full border border-gray border-2 rounded-md"}
                                        name="password"
                                        value={user.password}
                                        onChange={onChangeInput}
                                        type="text"
                                        placeholder="Insert your new password"
                                    />
                              
                           </div>
                           <div className="mt-5">
                                <label 
                                      htmlFor="confirm-password" 
                                      className="text-sm sm:text-base text-blue font-semibold">
                                         Confirm Password
                                </label><br/>
                                <input 
                                      className={error ? " bg-[#EEB4B0] p-[0.5em] placeholder-[#E46F67] sm:p-2 w-full border border-gray border-2 rounded-md" : "p-[0.5em] sm:p-2 w-full border border-gray border-2 rounded-md"}
                                      name="password"
                                      value={user.password}
                                      onChange={onChangeInput}
                                      type="text"
                                      placeholder="Insert your confirm password"
                                  />             
                            </div>
                            <button className="bg-blue w-full mt-4 p-2 rounded-md text-white font-semibold text-base" type="submit">Reset Password</button>
                      </form>
                </div> 
            </div>

           <Footer />
        </>
    )
}

export default ResetPassword