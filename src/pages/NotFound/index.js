import icon from "../../assets/img/find-project/not-found.svg"

function NotFound() {
    return (
        <div className="container mx-auto p-32 flex justify-center items-center ">
            <div className="text-center w-[40%] pb-28">
                 <section className="flex justify-center mb-7">
                    <img 
                        width={300}
                        height={300}
                        src={icon}
                        alt="not-found"
                    />
                 </section>
                 <h3 className="text-2xl font-semibold text-blue">Not Found</h3>
                 <p className="text-[14px] mt-1">Sorry, we couldn't find any results for your search. Please try again with different keywords.</p>
            </div>
        </div>
    )
}

export default NotFound