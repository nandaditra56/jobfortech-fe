import { useState } from "react"
import { Link, useNavigate } from "react-router-dom"
import Header from "../../layout/Header"
import Footer from "../../layout/Footer"
import image from "../../assets/img/signin/sign-up.svg"
import google from "../../assets/img/signin/logos_google-icon.svg"
import facebook from "../../assets/img/signin/logos_facebook.svg"

function SignUp() {
    const [ authenticated, setAuthenticated] = useState(false) 
    const [ user, setUser] = useState({email:'', password:''})
    const [ error, setError] = useState(false)
    const [ message, setMessage] = useState("");
    const navigate = useNavigate();

    // mock data 
    const emailData = "john@gmail.com"
    const passwordData = "test123"

    const onChangeInput = (e) => {
        const {name, value} = e.target
        setUser({ ...user, [name]: value })
    }

    const validateData = (email, password) => {
        if(email === '' && password === '') {
           setError(true)
           return false
        }
        
        if(email === emailData && 
           password === passwordData) {
           setAuthenticated(true)
           localStorage.setItem('user', user)
           return true
        }

        setMessage("Wrong email or password, please make sure that your email and password are correct.")
        return false
    }

    const submitHandleForm = (e) => {
        e.preventDefault()

        if(!validateData(user.email, user.password)) {
           navigate('/signin')
        }

        if(validateData(user.email, user.password)) {
           navigate('/')
        }
    }

   return (
    <>
      <Header />
        
      <div className="container mx-auto px-9 sm:px-12 md:px-32 ">
            <div className="grid grid-cols-2 py-8 ms:py-12 md:py-32">
                <div className="col-span-2 md:col-span-1">
                    <h1 className="text-blue text-2xl sm:text-4xl font-bold">Sign up</h1>
                    <p className="text-sm sm:text-xl font-medium my-5 mr-2">Get started on your freelance journey by signing up for <Link to={"/"} className="text-blue"><b>jobfortech</b></Link>  today.</p>
                    <div className="border border-1 mt-3 sm:mr-8 rounded-md">
                        <div className="px-4 sm:py-8 sm:px-9">
                                 <form onSubmit={submitHandleForm}>
                                   <div className="flex gap-5 mt-4 sm:mt-5">
                                        <div className="w-[100%]">
                                            <label 
                                                htmlFor="email" 
                                                className="text-blue text-sm sm:text-base font-semibold">
                                                    First Name
                                            </label><br />
                                            <input 
                                                className="p-[0.5em] sm:p-2 w-full text-sm border border-gray border-2 rounded-md"
                                                name="email" 
                                                value={user.email}
                                                onChange={onChangeInput}
                                                type="text"
                                                placeholder="Insert your first name"
                                                /><br /> 
                                        </div>

                                        <div className="w-[100%]">
                                            <label 
                                                htmlFor="email" 
                                                className="text-blue text-sm sm:text-base font-semibold">
                                                    Last Name
                                            </label><br />
                                            <input 
                                                className="p-[0.5em] sm:p-2 w-full text-sm border border-gray border-2 rounded-md"
                                                name="email" 
                                                value={user.email}
                                                onChange={onChangeInput}
                                                type="text"
                                                placeholder="Insert your last name"
                                                /><br /> 
                                        </div>
                                    </div>
                                    <div className="mt-4 sm:mt-5">
                                        <label 
                                            htmlFor="email" 
                                            className="text-blue text-sm sm:text-base font-semibold">
                                                Email
                                        </label><br />
                                        <input 
                                            className="p-[0.5em] sm:p-2 w-full text-sm border border-gray border-2 rounded-md"
                                            name="email" 
                                            value={user.email}
                                            onChange={onChangeInput}
                                            type="text"
                                            placeholder="Insert your email"
                                            /><br /> 
                                    </div>
                                    <div className="mt-5">
                                        <label 
                                            htmlFor="password" 
                                            className="text-sm sm:text-base text-blue font-semibold">
                                                Password
                                        </label><br/>
                                        <input 
                                            className={error ? " bg-[#EEB4B0] p-[0.5em] placeholder-[#E46F67] sm:p-2 w-full border border-gray border-2 rounded-md" : "p-[0.5em] sm:p-2 w-full border border-gray border-2 rounded-md"}
                                            name="password"
                                            value={user.password}
                                            onChange={onChangeInput}
                                            type="text"
                                            placeholder="Insert your password"
                                            />
                                       {error === true ? <p className="text-xs text-[#BD251C] py-1">*Please fill out all required fields before submitting the form.</p> : ""}
                                         
                                    </div>
                                    <div className="mt-5">
                                        <label 
                                            htmlFor="confirm-password" 
                                            className="text-sm sm:text-base text-blue font-semibold">
                                                Confirm Password
                                        </label><br/>
                                        <input 
                                            className={error ? " bg-[#EEB4B0] p-[0.5em] placeholder-[#E46F67] sm:p-2 w-full border border-gray border-2 rounded-md" : "p-[0.5em] sm:p-2 w-full border border-gray border-2 rounded-md"}
                                            name="password"
                                            value={user.password}
                                            onChange={onChangeInput}
                                            type="text"
                                            placeholder="Insert your password"
                                            />
                                       {error === true ? <p className="text-xs text-[#BD251C] py-1">*Please fill out all required fields before submitting the form.</p> : ""}
                                         
                                    </div>
                            
                                    <button className="bg-blue w-full p-2 rounded-md text-white font-semibold text-base mt-4" type="submit">Sign up</button>

                                    <div className="flex my-5 text-center">
                                        <hr className="border border-3 border-gray m-auto w-full" /><span className="w-80 m-auto text-xs">or sign up with</span><hr className="border border-3 border-gray m-auto w-full"/>
                                    </div>
                                
                                    <div className="flex sm:flex-row flex-col gap-4">
                                        <button className="w-full p-2 flex border border-gray rounded-md justify-center">
                                             <img 
                                                className="mx-3" 
                                                src={google} 
                                                alt="google-icon.png"/> 
                                                Google
                                        </button>
                                        <button className="w-full p-2 flex border border-gray rounded-md justify-center">
                                             <img 
                                                className="mx-3" 
                                                src={facebook} 
                                                alt="facebook-icon.png"/>
                                                 Facebook
                                        </button>
                                    </div>
                                    <p className="font-medium text-center mt-5 text-xs sm:text-sm">Already have an account? <b className="text-blue">Sign In</b></p>
                                 </form>
                            </div>
                        </div>
                     </div>
                     <div className="hidden md:block md:col-span-1 md:p-5">
                         <img
                            className="w-full" 
                            src={image} 
                            alt="ilustration-signin.png"/>
                     </div>
                 </div>
       </div>

      <Footer />
    </>
   )
}

export default SignUp