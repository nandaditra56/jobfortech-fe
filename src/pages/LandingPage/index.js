import { useState, useEffect } from "react"
import { useSelector } from "react-redux"
import Header from "../../layout/Header"
import Footer from "../../layout/Footer"
import CategoryCardItem from "../../components/CategoryCardItem"
import JobOfferItem from "../../components/JobOfferItem"
import PopularJobItem from "../../components/PopularJobItem"
import job from "../../../src/assets/img/undraw_job_offers.png"
import person1 from "../../assets/img/testimonial/sarah-kim.png"
import person2 from "../../assets/img/testimonial/michael-lee.png"
import person3 from "../../assets/img/testimonial/john-smith.png"
import quote1 from "../../assets/img/testimonial/quote-1.png"
import quote2 from "../../assets/img/testimonial/quote-2.png"
import "../../index.css"

function LandingPage() {
   const [current, setCurrent] = useState(0) 
   const projectCategories = useSelector(state => state.projectCategories)
   const popularTalentItems = useSelector(state => state.popularTalents)
   const jobOfferItems = useSelector(state => state.jobOffers)
   
   const prevButton = () => { 
       setCurrent((prev) => prev-1)  
   } 

   const nextButton = () => {
       setCurrent((prev)=> prev+1)
   }

   const BackToZeroNumber = () => {
      if(current > 1 || current < 0) {
         setCurrent(0)
      }
   }

   useEffect(() => {  
      const element = document.getElementsByClassName('item')[0];
      if(current === 1) {
         element.classList.add('-translate-x-[30em]')
      } else if (current === 0) {
         element.classList.remove('-translate-x-[30em]')
      }

      BackToZeroNumber()
   })

   console.log(window.innerWidth)

   const container = "container mx-auto px-9"
   console.log(projectCategories)
   return (
        <>
          <Header />
          <main>
             <div className="md:container md:mx-auto grid grid-cols-7 bg-radial-new bg-no-repeat md:bg-top-1 sm:bg-top-3 md:bg-right-top">
                 <div className="col-span-7 md:col-span-3 px-10 sm:px-20 md:pl-32 py-8 md:py-16">
                   <h1 className="font-bold text-blue text-2xl sm:text-4xl">Discover Your Next Project & Job</h1>
                   <p className="text-[12px] sm:text-xl md:text-2xl my-1">The Ultimate Job Platform for Talented Professionals</p>

                   <div className="flex justify-center items-center md:block">
                       <button className="rounded-full text-center bg-blue text-white text-[12px] md:text-lg py-2 px-8 md:px-11 mt-10">Get Started</button>   
                   </div>
                 </div>

                 <div className="col-span-7 px-20 md:col-span-4 relative ">
                   <img className="md:absolute bottom-0 right-14  md:w-9/12" src={job} alt="img-jumbotron.png"/>
                 </div>
             </div>

             <div className={`${container} md:px-32 py-2 md:py-6`}>
                  <h1 className="font-bold text-[16px] md:text-2xl text-black-300 my-4">Find Project by Category</h1>
                  <div className="grid grid-cols-2 sm:grid-cols-3 mx-auto md:grid-cols-4 gap-5">
                       {projectCategories.map((category, i) => 
                           <CategoryCardItem key={i} props={category}/> 
                       )}
                  </div>
             </div>

             <div className={`${container} md:px-32 py-5 md:py-10`}>
                  <h1 className="font-bold text-[16px] md:text-2xl text-black-300 my-4">Popular Job Offer</h1>
                  <div className="grid grid-cols- 1 sm:grid-cols-2 md:grid-cols-3 gap-5">
                       { jobOfferItems.map((offer, index)=> 
                          <JobOfferItem
                             key={index} 
                             props={offer}
                          />
                       )}
                  </div>
                  <div className="mt-5 flex justify-center items-center">
                     <button className="border border-2 border-blue px-16 py-2  text-blue text-sm font-semibold rounded-full">See More</button>
                  </div>
             </div>

             <div className={`${container} sm:px-2 md:px-32`}>
                  <div className="rounded-md pb-5 shadow-xl">
                      <div className="py-4 text-center">
                         <h1 className="font-bold text-[16px] md:text-2xl text-black-300">Find Project by Popular Talent Category</h1>
                         <p className="font-regular text-[12px] md:text-base py-2 md:mx-[27rem]">Find specific talents you need from our talent pool. Talk to our consultant to start outsourcing now.</p>
                      </div>
                      <div className="h-[31em] overflow-y-auto">
                        {popularTalentItems.map((item, index)=> 
                             <PopularJobItem 
                                key={index} 
                                props={item}
                             />
                        )}
                     </div>
                  </div>
             </div>

             <div className={`${container} sm:px-2 md:px-32 pb-16 mt-12`}>
                  <h1 className="font-bold text-base sm:text-2xl text-black-300 font-bold">Testimonials</h1>
                  <p className="text-xs sm:text-base md:text-xl font-medium py-4">
                   Find out why talented professionals everywhere choose <b className="font-bold text-blue">jobfortech</b> 
                  <br className="hidden md:block"/>to achieve their career goals - read testimonials from our satisfied users today.</p>     
                  <div className="w-full h-full mt-2 relative">
                        <div className="overflow-hidden">  
                           {current === 0 ? 
                             <button
                               onClick={nextButton}
                               className="absolute top-[12rem] right-0 bg-blue rounded-full py-2 px-3 z-10"
                               >
                                  <span 
                                  className="material-icons-outlined text-white">
                                        arrow_forward
                                  </span>
                             </button>  
                            : 
                              <button
                                 onClick={prevButton}
                                 className="absolute top-[12rem] left-0 bg-blue rounded-full py-2 px-3 z-10"
                              >
                                 <span 
                                 className="material-icons-outlined text-white">
                                 arrow_back
                                 </span>
                              </button>
                           }
                           <div className="item flex w-screen scroll-smooth gap-5 z-0 ease-in-out duration-200"> 
                              <div className="carousel-item w-64 md:w-538 py-5 md:px-4 bg-purple border rounded-2xl">
                                    <div className="relative mt-0 md:mt-3 mx-1">
                                       <img 
                                          className="w-6 md:w-10"
                                          src={quote1}
                                          alt="quote left"/>
                                       <p className="text-white text-base md:text-3xl font-medium px-7 md:px-10">Using <b className="text-blue">jobfortech</b> has made it easy for me to find quality projects and build my freelance career.</p>
                                       <img 
                                          src={quote2} 
                                          className="w-6 md:w-10 float-right"
                                          alt="quote right"/>
                                    </div>
                                    <div className="mx-2 pt-28">
                                       <hr className="border-2 border-blue-500 cursor-pointer rounded-full mb-3 md:mb-5"/>
                                       <img
                                          className="w-10 md:w-14 float-left mr-2" 
                                          src={person1} 
                                          alt="icon-user"/>
                                       <h5 className="text-white text-[16px] font-bold md:pt-1 m-0">Sarah Kim</h5>
                                       <span className="text-[10px] mt-1 text-white">Sunray Software</span>
                                    </div>
                              </div>
                              <div className="carousel-item w-64 md:w-538 py-5 md:px-4 bg-purple border rounded-2xl">
                                    <div className="relative mt-0 md:mt-3 mx-1">
                                       <img 
                                         className="w-6 md:w-10" 
                                         src={quote1}
                                         alt="quote left"/>
                                       <p className="text-white text-base md:text-3xl font-medium px-7 md:px-10">I'm impressed by the level of support and guidance provided by <b className="text-blue">jobfortech </b> to help freelancers succeed.</p>
                                       <img 
                                         src={quote2} 
                                         className="w-6 md:w-10 float-right"
                                         alt="quote right"/>
                                    </div>
                                    <div className="mx-auto mt-12 md:mt-20 pt-10">
                                       <hr className="md:border-2 border-blue-500 cursor-pointer rounded-full mb-5"/>
                                       <img 
                                          className="w-10 md:w-14 float-left ml-6 mr-2"
                                          src={person2} 
                                          alt="icon-user.png"/>
                                       <h5 className="text-white font-bold pt-1">Michael Lee</h5>
                                       <span className="text-white">BlueRock Consulting</span>
                                    </div>
                              </div>
                              <div className="carousel-item w-64 md:w-538 py-5 md:px-4 bg-purple border rounded-2xl">
                                    <div className="relative mt-0 md:mt-3 mx-1">
                                       <img 
                                          className="w-6 md:w-10" 
                                          src={quote1}
                                          alt="quote left"/>
                                       <p className="text-white text-base md:text-3xl font-medium px-7 md:px-10">I never realized how many great opportunities were out there until I started using <b className="text-blue">jobfortech</b> to find freelance work.</p>
                                       <img 
                                         src={quote2} 
                                         className="w-6 md:w-10 float-right"
                                         alt="quote right"/>
                                    </div>
                                    <div className="mx-auto mt-10 pt-10">
                                       <hr className="md:border-2 border-blue-500 cursor-pointer rounded-full mb-5"/>
                                       <img
                                          className="w-10 md:w-14 float-left ml-6 mr-2" 
                                          src={person3} 
                                          alt="icon-user.png"/>
                                       <h5 className="text-white font-bold pt-1">John Smith</h5>
                                       <span className="text-white">BlueRock Consulting</span>
                                    </div>
                              </div>
                              
                           </div>
                        </div>
                  </div>
                  
             </div>
          </main>

          <Footer />
        </>
    )
}

export default LandingPage