import { useState } from "react"
import { Link } from "react-router-dom"
import Footer from "../../layout/Footer"
import Header from "../../layout/Header"
import google from "../../assets/img/signin/logos_google-icon.svg"
import facebook from "../../assets/img/signin/logos_facebook.svg"
import mail from "../../assets/img/forgot-password/email.png"

function ForgotPassword() {
    const [email, setEmail] = useState('')
    const [error, setError] = useState(false)
    const [active, setActive] = useState(false)
    const [message, setMessage] = useState('')

    const dummyEmail = "nandaditra56@gmail.com"

    const handleChange = (e) => {
        setEmail(e.target.value)   
    }

    const validateData = () => {    
       if(!email.includes("@")) {
          setMessage("Invalid email format")
          setError(true)
          return false
       } else if (email !== dummyEmail) {
          setMessage("Email does not exist. Make sure your email already registered in our website.")
          setError(true)  
          return false 
       }
       
       if(email === '') {
          return false
       }
       
       return true
    }

    const handleSubmit = (e) => {
       e.preventDefault()
     
       if(validateData()) {
          setActive(true)
       }
    }

    console.log(active)

    if(active) {
       return (
          <>
            <Header/>
                <div className="container mx-auto px-9 sm:px-2 md:px-32 py-32 md:py-40 bg-radial-auth bg-no-repeat bg-center bg-auto">
                  <div className="mx-auto my-10 sm:my-2 md:my-36 border border-gray border-3 p-3 sm:p-9 w-full sm:w-9/12 text-center rounded-xl shadow-md item-center justify-center bg-white">
                          <h1 className="text-xl sm:text-3xl font-bold text-blue my-2">Email Sent!</h1>
                          <img
                              className="mx-auto w-1/3 sm:w-1/12" 
                              src={mail} 
                              alt="email.png"/>
                          <p className="text-xs sm:text-xl font-medium text-left my-5">We already sent you link to reset your password, please kindly check your inbox to proceed.  If you have not received the email after few minutes, please check your spam folder.</p>
                          <Link to={'/signin'}><button className="bg-blue font-semibold text-sm text-white w-full rounded-md p-2 ms:p-3 mt-1">Sign In</button></Link>
                  </div>
                </div>   
            <Footer/>
           </>
         )
    }

    return (
       <>
         <Header/>
            <div className="container mx-auto px-9 sm:px-2 md:px-32 py-14 sm:py-32 md:py-40 bg-radial-auth bg-no-repeat bg-center"> 
                <div className="mx-auto my-14 bg-white border border-gray border-3 p-5 ms:p-10 md:w-1/2 text-center rounded-xl shadow-md justify-center">
                      <h1 className="text-xl sm:text-3xl font-bold text-blue my-2">Forgot Password</h1>
                      <p className="text-left text-xs sm:text-xl mt-5">Enter your email below to receive your password reset construction</p>
                      <form className="text-left mt-3" onSubmit={handleSubmit}> 
                          <label className="text-blue text-sm font-semibold" htmlFor="email">Email</label> <br/>
                          <input 
                              className={error===true ? "border border-2 p-2 text-[#E46E67] bg-[#eeb4b0] rounded-md w-full" : "border border-2 p-2 text-gray bg-white rounded-md w-full"}
                              name="email" 
                              value={email}
                              onChange={handleChange}
                              type="text"
                              placeholder="Insert your email"/>

                          <p className={error ? 'text-[12px] text-[#bc251c]': 'hidden'}>{error ? message : ''}</p>
                          <button 
                             className="bg-blue font-semibold text-sm text-white w-full rounded-md p-2 sm:p-3 mt-5"
                             type="submit">   
                             Reset Password
                          </button>
                      </form>

                      <div className="flex my-2">
                          <hr className="border border-3 border-gray my-5 w-full" /><span className="w-80 m-auto text-xs">or sign in with</span><hr className="border border-3 border-gray my-5 w-full"/>
                      </div>

                      <div className="flex flex-col sm:flex-row gap-3">
                        <button className="w-full flex justify-center p-2 border border-gray rounded-md">
                              <img 
                                className="px-3" 
                                src={google} 
                                alt="google-icon"/> 
                                Google
                        </button>
                        <button 
                              className="w-full flex justify-center p-2 border border-gray rounded-md"
                               >
                              <img className="px-3" 
                                  src={facebook} 
                                  alt="facebook-icon"/> Facebook
                        </button>
                      </div>

                      <p className="text-xs mt-5">Didn't have an account? <b className="font-bold text-blue">Sign Up</b></p>
                </div> 
            </div>
         <Footer/>
       </>
    )
}

export default ForgotPassword