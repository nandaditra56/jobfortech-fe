import AppliedSignIn from "../../../components/AppliedSignIn"

const description = "Sign in to our website to get your next job!"

function AppliedJob(){
   return (
     <AppliedSignIn description={description}/>
   )
}

export default AppliedJob