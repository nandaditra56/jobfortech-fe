import React from 'react'
import Header from '../../layout/Header'
import Footer from '../../layout/Footer'
import search from '../../assets/img/find-project/search-icon.svg'
import image from '../../assets/img/find-project/find-project-bg.svg'
import { Outlet, NavLink } from 'react-router-dom'

function FindJob() {
    return (
        <>
        <Header />
        
        <div className="pb-10 z-10">
            <div className="container mx-auto md:flex bg-radial-new bg-no-repeat md:bg-top-1 sm:bg-top-3 md:bg-right-top">
              <div className="w-[100%] md:w-[40%] pl-9 sm:pl-20 md:pl-32 md:py-10">
                 <h1 className="text-blue text-[20px] sm:text-[40px] font-bold">Find Your Dream Job Here!</h1>
                 <p className="font-medium text-xs sm:text-2xl">Discover your dream job with our easy-to-use search and application tools</p>
              </div>
              <div className="w-[100%] md:w-[60%] md:relative z-20">              
                  <img 
                     src={image} 
                     alt="image"
                     className="absolute -bottom-10 right-40 -z-50"/>
              </div>
            </div>
            <div className="container mx-auto px-5 md:px-32 mt-10">
                 <div className="border-2 rounded-xl shadow-lg">
                      <div className="p-5">
                          <ul className="flex text-xs sm:text-xl font-semibold text-black-300">
                              <li className="px-3 sm:px-7 sm:py-2 hover:border-b-4 border-blue hover:text-blue"><NavLink to={"/find-job/browser-job"}>Browse</NavLink></li>
                              <li className="px-3 sm:px-7 sm:py-2 hover:border-b-4 border-blue hover:text-blue"><NavLink to={"/find-job/applied-job"}>Applied</NavLink></li>
                              <li className="px-3 sm:px-7 sm:py-2 hover:border-b-4 border-blue hover:text-blue"><NavLink to={"/find-job/saved"}>Saved</NavLink></li>
                          </ul>
                          <button className="w-full text-blue border p-2 mt-4 border-2 border-blue rounded-xl font-bold sm:hidden">
                              Filter
                          </button>
                          <div className="bg-[#F5F5F5] mt-3 rounded-2xl hidden sm:block">
                              <div className="py-3 px-7">
                                  <form className="flex flex-row gap-5">
                                      <div className="flex md:flex-row flex-col w-[100%] gap-4">
                                          <div className="md:w-[100%]">
                                              <p className="text-xs text-[#404040]">Search</p>
                                              <div className="w-full flex bg-white border-2 border-[#E0E0E0] rounded-lg">
                                                  <img 
                                                      src={search}
                                                      alt="icon-search"
                                                      width={50}
                                                      className='px-4'
                                                  />
                                                  <input
                                                      name="name"
                                                      type="text"
                                                      placeholder="search project name, Talent, Company etc"
                                                      className="border-white bg-white outline-none text-[12px] md:text-[14px] focus:ring-0 w-full py-2 rounded-lg"
                                                  />
                                              </div>                                           
                                          </div>
                                          <div className="flex gap-4 mt-2 md:mt-0 w-full">
                                              <div className="w-[50%]"> 
                                                  <p className="text-xs text-[#404040]">Category</p>
                                                  <select className="bg-white focus:ring-0 w-full py-2.5 px-2 border border-2 rounded-lg font-bold text-black">
                                                      <option value="all">All</option>
                                                      <option value="saab">Saab</option>
                                                      <option value="opel">Opel</option>
                                                      <option value="audi">Audi</option>
                                                  </select>
                                              </div>
                                              <div className="w-[50%]"> 
                                                  <p className="text-xs text-[#404040]">Talent</p>
                                                  <select className="bg-white focus:ring-0 w-full py-2.5 px-2 border border-2 rounded-lg font-bold text-black">
                                                      <option value="all">All</option>
                                                      <option value="saab">Saab</option>
                                                      <option value="opel">Opel</option>
                                                      <option value="audi">Audi</option>
                                                  </select>
                                              </div>
                                          </div>
                                      </div>
                                      <button 
                                          type="submit"
                                          className="px-10 py-1 my-10 md:my-2 bg-blue rounded-lg text-white"
                                          >
                                          Filter
                                      </button>
                                  </form>
                              </div> 
                          </div>
                      </div>
                 </div>
              </div>

              <Outlet />
              {/* <NotFound /> */}
        </div>

        <Footer />
    </>
    )
}

export default FindJob;