import { useSelector } from "react-redux"
import JobItem from "../../../components/JobItem"

function BrowserJob() {
    const jobs = useSelector(state => state.jobs) 

    return (
        <div className="container mx-auto px-7 sm:px-5 md:px-32 my-5">
            <div className="grid grid-cols-1 md:grid-cols-3 gap-4">
                { jobs.map((item, index) => 
                    <JobItem 
                        key={index}
                        props={item}
                    />
                )}
            </div>

            <div className="flex items-center justify-center my-8">
                <hr className="py-[0.5px] md:py-[1px] bg-[#D9D9D9] w-[12%] sm:w-[33%] md:w-[42%]"/>
                <button className="border-2 border-blue px-16 sm:px-12 sm:py-1 md:py-2 text-blue font-bold rounded-full mx-2">Load More</button>
                <hr className="py-[0.5px] md:py-[1px] bg-[#D9D9D9] w-[12%] sm:w-[33%] md:w-[42%]"/>
            </div>
        </div>
    )
}

export default BrowserJob