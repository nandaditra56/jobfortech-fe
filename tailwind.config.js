module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    extend: {
      colors:{
        'blue':'#0371D8',
        'green':'#277D3E',
        'orange':'#F47133',
        'gray-300':'#EBF0ED',
        'black-300':'#404040',
        'purple':'#2F2E41'
      },
      fontSize:{
        'xs':['13px','17px'],
        '4xl':['40px', '48px']
      },
      backgroundImage:{
        'noise-tecture': "url('/src/assets/img/background/Noise-Texture.png')",
        'radial-background':"url('/src/assets/img/landing-page/bg-landingpage.svg')",
        'radial-new':"url('/src/assets/img/landing-page/bg-landingpage.svg')",
        'radial-auth':"url('/src/assets/img/bg-auth-features/bg-cicle-blue.svg')",
        'gradient-radial': 'radial-gradient(ellipse_at_right,_var(--tw-gradient-stops))',
        'find-project': "url('/src/assets/img/find-project/find-project-bg.svg')"
      },
      backgroundPosition: {
        'top-4': 'right 10rem top 1rem',
        'top-1': 'right -10rem top -5rem',
        'bottom-4': 'center top 15rem',
        'bottom-8': 'center top 15rem',
      },
      width:{
        '538':'1200rem',
        'screen': '105.5rem'
      }
    },
  },
  plugins: [],
}

